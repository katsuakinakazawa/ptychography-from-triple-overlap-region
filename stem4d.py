# ver3
# CLAP1も読むように変更。

import os
import glob
import math
import shutil
import numpy as np
import configparser
import hyperspy.api as hs
from concurrent.futures import ProcessPoolExecutor
from scipy.optimize import curve_fit
from PIL import Image
from multiprocessing import Pool


class Image4d:
    def __init__(self, foldername):
        if foldername[-1] != '/':
            foldername += '/'
        self.foldername = foldername
        self.linename=self.foldername+'Lines/'
        self.processname=self.foldername+'Process/'
        os.makedirs(self.processname, exist_ok=True)
        if len(glob.glob(self.foldername + '*.hdr')) != 0:
            self.read_hdrfile()
        elif len(glob.glob(self.foldername + 'parameters.txt')) != 0:
            self.read_txtfile()
        else:
            self.read_simfile()

    def read_hdrfile(self):
        """
        read .hdr file to obtain variables.
        .hdr file is made at the acquisition by 4DCanvas.
        """
        name = glob.glob(self.foldername+'*.hdr')
        shutil.copy(name[0], name[0][:-4]+'.ini')
        inifile = configparser.ConfigParser()
        inifile.read(glob.glob(self.foldername+'*.ini'),'UTF-8')
        readoutMode = inifile.get('measurementInfo', 'readoutMode')
        self.bin = int(readoutMode[readoutMode.find("bin:")+4
                                  :readoutMode.rfind(",")]) # self.bin is the number of hardware binning
        self.kx = int(readoutMode[readoutMode.find("windowing:")+10
                                     :readoutMode.find("x")])
        self.ky = int(int(readoutMode[readoutMode.find("x")+1
                                     :-1])/self.bin)
        self.comment = inifile.get('measurementInfo', 'comment')
        self.sampleid = inifile.get('measurementInfo', 'sampleId')
        self.dwell = inifile.getfloat('measurementInfo', 'dwellTimeMicroseconds')*1e-6 # (s)
        stemImageSize = inifile.get('measurementInfo', 'stemImageSize')
        self.x = int(int(stemImageSize[:stemImageSize.find("x")])) 
        self.y = int(int(stemImageSize[stemImageSize.find("x")+1:]))
        if self.x*self.y == 0:
            framenum = inifile.getint('measurementInfo','signalFrames')
            if np.sqrt(framenum)%1 == 0:
                self.y = np.int(np.sqrt(framenum))
                self.x = np.int(np.sqrt(framenum))
            else:
                self.y = np.int(np.sqrt(framenum*2))
                self.x = np.int(np.sqrt(framenum/2))
        self.cl = inifile.getfloat('microscopeInfo','CL') * 1e-2 # (m) self.cl is the camera length
        self.clap1 = inifile.getfloat('microscopeInfo','CLAP1')# CL aperture
        self.beamcurrent = inifile.getfloat('microscopeInfo','beamCurrent') # (uA)
        self.accvol = inifile.getint('microscopeInfo','HV')*1e3 # (V)
        self.wavelength = 1.2264*1e-9/(
                          math.sqrt(self.accvol*(1+9.7846*1e-7*self.accvol)))  # (m)
        self.mag = inifile.get('microscopeInfo','magnification')
        if self.mag[-1] == 'k':#convert mag into int。
           self.mag = int(float(self.mag[1:-1]))*1e3
        elif self.mag[-1] == 'M':
           self.mag = int(float(self.mag[1:-1]))*1e6
        else:
           self.mag = int(float(self.mag[1:]))
        self.visualfield = 778.11*250*1e-6/self.mag # (m) 778.11 is a magic number.
        self.pixelsize = self.visualfield/self.x # (m)
        self.pixelsize_k_m = 2*np.sin(self.pixelsize_k_rad/2)/self.wavelength # (m-1)
        self.pixelsize_k_rad = np.arcsin(self.pixelsize_k_m*self.wavelength/2)*2 # (rad)

    def read_simfile(self):
        """
        read simulation files to obtain variables.
        simulation files are made at the simulation by Dr. Probe.
        """
        self.bin = 1
        celfile = self.foldername +'../../log/celslc_out0.log'
        msafile = self.foldername +'msa.prm'
        cf = open(celfile)
        clines = cf.readlines()
        cf.close()
        self.accvol = float(clines[16].split(' ')[3]) * 1e3 # (eV)
        self.wavelength = float(clines[17].split(' ')[3]) * 1e-12 # (m)
        try:
            celx = float(clines[32].split(' ')[6]) * 1e-9 # (m)
            cely = float(clines[32].split(' ')[7]) * 1e-9 # (m)
            celz = float(clines[32].split(' ')[9][:-2]) * 1e-9 # (m)
        except (IndexError, ValueError) as e:
            celx = float(clines[32].split(' ')[7]) * 1e-9 # (m)
            cely = float(clines[32].split(' ')[9]) * 1e-9 # (m)
            celz = float(clines[32].split(' ')[11][:-2]) * 1e-9 # (m)
        self.kx = round(celx/(float(clines[33].split(' ')[7])*1e-9))
        self.ky = round(cely/(float(clines[33].split(' ')[9][:-1])*1e-9))
        mf = open(msafile)
        mlines = mf.readlines()
        mf.close()
        self.semiangle = float(mlines[1].split(' ')[0]) #(mrad)
        self.x = int(float(mlines[31].split(' ')[0]))
        self.y = int(float(mlines[32].split(' ')[0]))
        repx = int(float(mlines[35].split(' ')[0]))
        repy = int(float(mlines[36].split(' ')[0]))
        self.kx *= repx
        self.ky *= repy
        self.visualfield = repx*celx
        self.pixelsize = self.visualfield/self.x
        self.pixelsize_k_m = 1/(celx*repx) # unit m-1
        self.pixelsize_k_rad = np.arcsin(self.pixelsize_k_m*self.wavelength/2)*2 #rad

    def make_tif_images(self, update=False):
        """
        Make .tiffiles from .frms6 files
        000.frms6 file is for calibration
        The tiffiles are saved at ./Lines/ folder

        Parameters
        ----------
        update : bool
            select update the existed tiffiles or not

        Return
        ------

        """
        if os.path.isdir(self.foldername+'Lines/') and update==False:
            pass
        else:
            self.numberofframesperfile = self.x #フレームとは行の事か・・・？
            frms6files=glob.glob(self.foldername+'*.frms6') # frms6ファイル名をリストで取得。
            offsetfile = list()
            imagefiles = list()
            for frms6file in frms6files:
                if '000' in frms6file[-9:]:
                    offsetfile.append(frms6file)# 000が付いたファイルは、ofsetにアペンド。
                else:
                    imagefiles.append(frms6file)#それ以外はimagefilesにアペンド
            self.read_frms6file(offsetfile[0], mode='offset')
            Image.fromarray(self.offset).save(self.foldername+'offsetMap.tif')#検出器のオフセットマップをtifで保存。
            imagefiles.sort()#念のためファイル名をソート。 番号順に入るとは限らないのかな・・・？
            os.makedirs(self.foldername+'Lines', exist_ok=True)
            self.cnt = 0 #適正に初期化されているか心配だったこの変数は、ここで初期化されていた。。。
            for imagefile in imagefiles:
                self.read_frms6file(imagefile, mode='frame')# この関数の中で、行ごとのtifの書き出しまでやる。
            del(self.cnt)

    def make_tif_images_multi(self, update=False):
        """
        Multi core version of "make_tif_images"

        Parameters
        ----------
        cpu_num : int
            the number of core for parallel computing
        update : bool
            select update the existed tiffiles or not

        Return
        ------
        """
        if os.path.isdir(self.foldername+'Lines/') and update==False:
            pass
        cpu_num = os.cpu_count()//2
        with Pool(processes=cpu_num) as p:
            p.map(self.make_tif_images,[update]*cpu_num)

    def change_settings(self, bin_x, bin_y, bin_kx, bin_ky):
        """
        Change_settings caused by binning
        The disignated folder for reading is changed to binned folder

        Parameters
        ----------
        bin_x : int
            the number of binning for xaxis(scanning direction)
        bin_y : int
            the number of binning for yaxis(scanning direction)
        bin_kx : int
            the number of binning for kxaxis(diffraction)
        bin_ky : int
            the number of binning for kyaxis(diffraction)

        Return
        ------
        None

        """
        self.x = self.x//bin_x
        self.y = self.y//bin_y
        self.ky = self.ky//bin_ky
        self.kx = self.kx//bin_kx
        self.pixelsize *= bin_x
        self.pixelsize_k_rad *= bin_kx
        #self.dwell *= bin_x*bin_y
        self.bin = 1
        self.linename = self.foldername+'Lines_y'+str(bin_y)+'_x'+str(bin_x)+'_ky'+str(bin_ky)+'_kx'+str(bin_kx) + '/'

    def make_bin_images(self, bin_x, bin_y, bin_kx, bin_ky):
        """
        Bin the pixels in real and reciprocal axes
        New tifimages are made at new folder 'Lines_y4_x4_ky4_kx4/' (the number 4 is changed as the bin number)

        Parameters
        ----------
        bin_x : int
            the number of binning for xaxis(scanning direction)
        bin_y : int
            the number of binning for yaxis(scanning direction)
        bin_kx : int
            the number of binning for kxaxis(diffraction)
        bin_ky : int
            the number of binning for kyaxis(diffraction)

        Return
        ------
        None

        """
        tifnames = glob.glob(self.linename + "*.tif")
        ys = np.zeros(len(tifnames))
        self.change_settings(bin_x, bin_y, bin_kx, bin_ky)
        ind = 0
        for name in tifnames:
            ys[ind] = int(name[name.rfind('line_')+5:name.rfind('.tif')])
            ind += 1
        shape = (self.x, bin_x, self.ky, bin_ky, self.kx, bin_kx)
        os.makedirs(self.linename, exist_ok=True)
        new_lineimage = np.zeros((self.x,self.ky,self.kx)).astype(np.float32)
        for i in range(len(tifnames)//bin_y):
            new_lineimage = np.zeros((self.x,self.ky,self.kx)).astype(np.float32)
            for j in range(bin_y):
                name = tifnames[np.where(ys==(i*bin_y+j))[0][0]]
                new_lineimage  += hs.load(name).data.astype(np.float32).reshape(shape).sum((1,3,5))
            imagesdata = hs.signals.Signal2D(new_lineimage.astype(np.float32))
            imagesdata.save(self.linename + 'line_'+str(i),
                            overwrite=True,
                            extension='tif')

    def read_frms6file(self, frms6file, mode='offset'):
        """
        Read frms6file and obtain .tif images
        New tifimages are made at new folder 'Lines_y4_x4_ky4_kx4/' (the number 4 is changed as the bin number)

        Parameters
        ----------
        frms6file: str
            filename to read
        mode : 'offset' or 'fram'e
            select mode offset or frame
            offset is for the read of offset(000.frms6)
            frame is for the read of signal (except for 000.frms6)

        Return
        ------
        None

        """
        filesize = os.path.getsize(frms6file)
        with open(frms6file, 'rb') as fid:
            data1 = np.fromfile(fid, 'H',count=2)
            data2 = np.fromfile(fid, 'c',count=4)
            data3 = np.fromfile(fid, dtype=np.uint8,count=80).astype('c')
            pixels = np.fromfile(fid, 'H', count=2)
            data4 = np.fromfile(fid, dtype=np.uint8,count=932)
            pixelnum = pixels[0]*pixels[1].astype(np.uint)
            lengthframerecord= 64 + pixelnum*2
            imagenum=int((filesize-1024)//lengthframerecord)
            if (filesize-1024)%lengthframerecord != 0:
                print('Error', frms6file, 'Byte Check')
            if mode == 'offset':
                images = np.zeros((self.ky, self.kx, imagenum),
                                  dtype=np.uint16)
                for i in range(imagenum):
                    images[:,:,i] = self.convert_image(fid)
                self.offset = np.average(images, axis=2).astype(np.uint16)
            elif mode == 'frame':
                imagesline = imagenum//self.x 
                if imagenum%self.x != 0:
                    print('Error', frms6file, 'Images Line Check')
                images = np.zeros((self.ky, self.kx, self.x))
                for l in range(imagesline):
                    for i in range(self.x):
                        images[:,:,i] = self.convert_image(fid)
                    images -= self.offset.reshape(self.ky, self.kx, 1)
                    images[np.where(images < 0)] = int(0)
                    imagedata = hs.signals.Signal2D(images.transpose(2,0,1).astype(np.int16))
                    imagedata.save(self.linename+'/line_'+str(self.cnt),
                                   overwrite=True,
                                   extension='tif')
                    self.cnt += 1
                    print(self.cnt,'/', self.y)

    def get_4Ddata(self, foldername):
        image4d = np.zeros((self.y,
                            thself.x,
                            self.ky,
                            self.kx//self.bin),
                            dtype = np.complex64)
        tifnames = glob.glob(foldername+'*.tif')
        cnt = 0
        for name in tifnames:
            print('read', cnt)
            y = int(name[name.rfind('line_')+5:name.rfind('.tif')])
            lineimage = np.array(hs.load(name).data)
            image4d[y, :, :, :] = self.bin_lineimage(lineimage)
            cnt += 1
        return image4d

    def convert_image(self, fid):
        frameheader = np.fromfile(fid,dtype=np.uint8,count=64)
        image = np.fromfile(fid, '<H', count=self.ky*self.kx).reshape(self.ky//2,
                                                                      self.kx*2)
        image = self.recompose_image(image)
        return image

    def recompose_image(self, imgin):
        imgout = np.zeros((self.ky, self.kx),dtype=np.int16)
        imgout[0:self.ky//2, 0:self.kx] = imgin[:,0:self.kx]
        imgout[self.ky//2:self.ky, 0:self.kx] = np.rot90(imgin[:,self.kx:self.kx*2], 2)
        return imgout

    def bin_lineimage(self, lineimage):
        lineimage_bin = lineimage.reshape(self.x,
                                          self.ky,
                                          self.kx//self.bin,
                                          self.bin).sum(3)
        return lineimage_bin

    def check_kspace(self):
        dis = np.sqrt(np.square(self.cx-self.x/2)+np.square(self.cy-self.y/2))
        dis_limit = self.x/2
        return ((dis_limit-np.max(dis))*self.pixelsize_k)

    def process_image_from_lines(self, imgfilter, power=1):
        tifnames = glob.glob(self.foldername + "Lines/*.tif")
        image = np.zeros((self.y, self.x))
        for name in tifnames:
            y = int(name[name.rfind('line_')+5:name.rfind('.tif')])
            if self.bin != 1:
                lineimage = self.bin_lineimage(hs.load(name).data)
            else:
                lineimage = hs.load(name).data
            image[y ,:] = (np.power(lineimage*imgfilter, power)).sum(2).sum(1)
        return image

    def process_average_diffraction(self):
        """
        Make the averaged diffraction image from full 4DSTEM data

        Parameters
        ----------
        None

        Return
        ------
        image : 2darray
            The averaged diffraction image

        """
        tifnames = glob.glob(self.linename+'*.tif')
        image = np.zeros((self.ky, self.kx//self.bin))
        for name in tifnames:
            print(name)
            y = int(name[name.rfind('line_')+5:name.rfind('.tif')])
            if self.bin != 1:
                lineimage = self.bin_lineimage(hs.load(name).data)
            else:
                lineimage = hs.load(name).data
            image += lineimage.sum(0)
        Image.fromarray(image).save(self.processname + 'mean.tif')
        return image

    def make_adf(self,
                 filts,
                 ys=np.array([0,0])):
        tifnames = glob.glob(self.linename+'*.tif')
        image = np.zeros((ys[1]-ys[0], self.x))
        bg = hs.load(self.processname +'bg.tif').data
        cnt = 0
        for y in range(ys[0], ys[1]):
            if self.bin != 1:
                lineimage = self.bin_lineimage(hs.load(f'{self.linename}line_{y}.tif').data)
            else:
                lineimage = hs.load(f'{self.linename}line_{y}.tif').data
            lineimage = lineimage.astype(np.float32)
            lineimage -= np.tile(bg, (self.x, 1, 1))
            images = lineimage*filts
            image[y-ys[0], :] = images.sum(1).sum(1)
            cnt += 1
        return image

    def make_eadf_multi(self,
                       r_in=0,
                       r_out=0,
                       azu_angles=np.array([[0,360]])):
        images= []
        cpu_num = os.cpu_count()//2
        if r_out == 0:
            r_in = self.ky//4
            r_out = self.ky//2
        filts = self.make_circularfilter(self.ky//2, self.kx//self.bin//2, r_in, r_out, azu_angles)
        filts -= self.make_circularfilter(self.ky//2, self.kx//self.bin//2, 0, self.ky//6, azu_angles)
        filts = filts.reshape((1, self.ky, self.kx//self.bin))
        with ProcessPoolExecutor(max_workers=cpu_num) as executor:
            for i in range(cpu_num):
                images.append(executor.submit(self.make_adf,
                                              filts,
                                              np.array((int(self.y/cpu_num*i),
                                                        int(self.y/cpu_num*(i+1))))).result())
        images = np.concatenate(images, axis=0)
        return images

    def make_adf_multi(self,
                       r_in=0,
                       r_out=0,
                       azu_angles=np.array([[0,360]])):
        images= []
        cpu_num = os.cpu_count()//2
        if r_out == 0:
            r_in = self.ky//4
            r_out = self.ky//2
        filts = self.make_circularfilter(self.ky//2, self.kx//self.bin//2, r_in, r_out, azu_angles)
        filts = filts.reshape((1, self.ky, self.kx//self.bin))
        with ProcessPoolExecutor(max_workers=cpu_num) as executor:
            for i in range(cpu_num):
                images.append(executor.submit(self.make_adf,
                                              filts,
                                              np.array((int(self.y/cpu_num*i),
                                                        int(self.y/cpu_num*(i+1))))).result())
        images = np.concatenate(images, axis=0)
        return images

    def process_af_from_lines(self,
                              r_in=0,
                              r_out=0,
                              azu_angles=np.array([[0,360]]),
                              thresh_min=0,
                              thresh_max=1e5,
                              dr=0,
                              dtheta=0,
                              filt=1):
        if r_out == 0:
            r_in = self.ky//2
            r_out = self.ky
        tifnames = glob.glob(self.linename+'*.tif')
        image = np.zeros((self.y, self.x))
        filters = np.zeros((self.x, self.ky, self.kx//self.bin))
        pixels = np.zeros((self.x))
        bg = hs.load(self.processname +'bg.tif').data
        cnt = 0
        for name in tifnames:
            y = int(name[name.rfind('line_')+5:name.rfind('.tif')])
            if self.bin != 1:
                lineimage = self.bin_lineimage(hs.load(name).data)
            else:
                lineimage = hs.load(name).data
            lineimage = lineimage.astype(np.float32)
            lineimage -= np.tile(bg, (self.x, 1, 1))
            lineimage[np.where(lineimage > thresh_max)] = thresh_max
            lineimage[np.where(lineimage < thresh_min)] = 0
            for x in range(self.x):
                cy = self.cy[y, x]
                cx = self.cx[y, x]
                filters[x, :, :] = self.make_circularfilter(cy,
                                                            cx,
                                                            r_in,
                                                            r_out,
                                                            azu_angles,
                                                            dr=dr,
                                                            dtheta=dtheta)
            images = lineimage*filt*filters
            image[y, :] = images.sum(1).sum(1)
            cnt += 1
            print(cnt,'/',self.y)
        return image

    def process_radius_from_lines(self,
                                  r_in,
                                  r_out,
                                  azu_angles=np.array([[0,360]]),
                                  thresh_min=0,
                                  thresh_max=1e5,
                                  dr=0,
                                  dtheta=0,
                                  filt=1):
        tifnames = glob.glob(self.linename+'*.tif')
        mean = hs.load(self.processname +'mean.tif').data/self.ky/self.kx
        image = np.zeros((self.y, self.x))
        filters = np.zeros((self.x, self.ky, self.kx//self.bin))
        pixels = np.zeros((self.x))
        cnt = 0
        for name in tifnames:
            y = int(name[name.rfind('line_')+5:name.rfind('.tif')])
            if self.bin != 1:
                lineimage = self.bin_lineimage(hs.load(name).data) - mean
            else:
                lineimage = hs.load(name).data - mean
            lineimage[np.where(lineimage > thresh_max)] = thresh_max
            lineimage[np.where(lineimage < thresh_min)] = 0
            for x in range(self.x):
                cy = self.cy[y, x]
                cx = self.cx[y, x]
                filters[x, :, :] = self.make_circularfilter(cy,
                                                            cx,
                                                            r_in,
                                                            r_out,
                                                            azu_angles,
                                                            dr=dr,
                                                            dtheta=dtheta)
            images = lineimage*filt*filters/(np.sum(lineimage*filters, axis=(1,2)).reshape(-1,1,1))
            image[y, :] = images.sum(1).sum(1)
            cnt += 1
            print(cnt,'/',self.y)
        return image

    def process_ave_from_lines(self, r_in, r_out, azu_angles):
        tifnames = glob.glob(self.foldername + "Lines/*.tif")
        image = np.zeros((self.y, self.x))
        filters = np.zeros((self.x, self.ky, self.kx))
        cnt = 0
        for name in tifnames:
            y = int(name[name.rfind('line_')+5:name.rfind('.tif')])
            if self.bin != 1:
                lineimage = self.bin_lineimage(hs.load(name).data)
            else:
                lineimage = hs.load(name).data
            for x in range(self.x):
                cy = self.cy[y, x]
                cx = self.cx[y, x]
                filters[x, :, :] = self.make_circularfilter(cy,
                                                            cx,
                                                            r_in,
                                                            r_out,
                                                            azu_angles)
            images = lineimage*filters
            image[y, :] = np.sum(images, axis=(1,2))/np.sum(filters, axis=(1,2))
            cnt += 1
            print(cnt,'/',self.y)
        return image

    def process_std_from_lines(self, r_in, r_out, azu_angles):
        tifnames = glob.glob(self.foldername + "Lines/*.tif")
        image = np.zeros((self.y, self.x))
        filters = np.zeros((self.x, self.ky, self.kx))
        cnt = 0
        for name in tifnames:
            y = int(name[name.rfind('line_')+5:name.rfind('.tif')])
            if self.bin != 1:
                lineimage = self.bin_lineimage(hs.load(name).data)
            else:
                lineimage = hs.load(name).data
            for x in range(self.x):
                cy = self.cy[y, x]
                cx = self.cx[y, x]
                filters[x, :, :] = self.make_circularfilter(cy,
                                                            cx,
                                                            r_in,
                                                            r_out,
                                                            azu_angles)
            images = lineimage*filters
            image[y, :] = np.std(images, axis=(1,2))
            cnt += 1
            print(cnt,'/',self.y)
        return image

    def correct_images(self,
                       images,
                       pixels,
                       thresh_noise,
                       thresh_limit):
        correct_cnt = np.zeros_like(pixels)
        images[np.where(images <= thresh_noise)] = 0
        images[np.where(images >= thresh_limit)] = thresh_limit
        for z in range(images.shape[0]):
            image = images[z,:,:]
            ratio = np.sum(image == thresh_limit)/pixels[z]
            if ratio >= 0.9:
                correct_cnt = np.inf
            elif ratio >= 0.5:
                correct_cnt = np.inf
            else:
                average = np.sum(image[np.where(image < thresh_limit)])/pixels[z]
                correct_cnt = (average*(0.5-ratio)+thresh_limit*ratio)/0.5
        return correct_cnt

    def make_darray(self, cy, cx, ratio=0.0, rot=0):
        '''
        ratio (float):
        value for add
        
        '''
        a_filter = np.zeros((self.ky, self.kx//self.bin))
        x_array = np.arange((self.kx//self.bin))-(cx)
        x_array = np.tile(x_array, (self.kx//self.bin,1))
        y_array = (np.arange((self.ky))-cy).reshape(-1,1)
        y_array = np.tile(y_array, (1, self.ky))
        d_array = np.sqrt(np.square(x_array)
                         +np.square(y_array))
        a_array = np.arctan2(y_array*-1, x_array)*180 /np.pi
        a_array[np.where(a_array <=0)] += 360
        d_array += d_array * ratio*np.cos((a_array-rot)*2*np.pi/180)
        y_array = y_array.astype(np.float64)
        x_array = x_array.astype(np.float64)
        return y_array, x_array, d_array, a_array

    def make_darray4d(self,ratio=1):
        y_array = (np.arange((self.y))-self.y//2).reshape(-1,1,1,1)
        y_array = np.tile(y_array, (1, self.x, self.ky, self.kx))
        x_array = (np.arange(self.x)-self.x//2).reshape(1,-1,1,1)
        x_array = np.tile(x_array, (self.y, 1, self.ky, self.kx))
        ky_array = (np.arange((self.ky))-self.ky//2).reshape(1,1,-1,1)
        ky_array = np.tile(ky_array, (self.y, self.x, 1, self.kx//self.bin))
        kx_array = (np.arange(self.kx//self.bin)-self.kx/self.bin//2).reshape(1,1,1,-1)
        kx_array = np.tile(kx_array, (self.y, self.x, self.ky, 1))
        d_array = np.sqrt(np.square(x_array)
                         +np.square(y_array)
                         +np.square(ky_array*ratio)
                         +np.square(kx_array*ratio))
        return d_array

    def gaussian4d(self,sigma=1,ratio=1):
        dis = self.make_darray4d(ratio)
        gauss = np.exp(-1*(dis/sigma)**2)
        gauss /= np.sum(gauss)
        return gauss

    def measure_center(self, calib=True):
        if os.path.isfile(self.processname + 'mean_nbg.tif'):
            mean = hs.load(self.processname + 'mean_nbg.tif').data
        elif os.path.isfile(self.processname + 'mean.tif'):
            mean = hs.load(self.processname + 'mean.tif').data
        else:
            mean = self.process_average_diffraction()
        if calib == True:
            mean = np.where(mean>np.max(mean)*2/3, 1, 0)
        y, x, _, _ = self.make_darray(0, 0)
        cen_ky = np.average(y*mean)/np.average(mean)
        cen_kx = np.average(x*mean)/np.average(mean)
        self.cy = np.ones((self.y, self.x))*cen_ky
        self.cx = np.ones((self.y, self.x))*cen_kx
        print(cen_ky, cen_kx)

    def measure_center_full(self, calib=True, yr=np.array((0,0))):
        self.cy = np.zeros((self.y, self.x//self.bin))
        self.cx = np.zeros((self.y, self.x//self.bin))
        ys, xs, _, _ = self.make_darray(0, 0)
        if yr[1] == 0:
            yr[1] = self.y
        for y in range(yr[0], yr[1]):
            print(y)
            for x in range(self.x//self.bin):
                image = self.get_image(y, x)
                if calib == True:
                    image = np.where(image>np.max(image)*7/10, 1, 0)
                    self.cy[y-yr[0], x] = np.average(ys*image)/np.average(image)
                    self.cx[y-yr[0], x] = np.average(xs*image)/np.average(image)
        Image.fromarray(self.cy).save(self.processname + 'centroidy.tif')
        Image.fromarray(self.cx).save(self.processname + 'centroidx.tif')

    def make_circularfilter(self,
                            cy,
                            cx,
                            r_in,
                            r_out,
                            azu_angles = np.array([[0, 360]]),
                            unit='pixel',
                            dr=0,
                            dtheta=0):
        """
        make circularfilter

        Parameters
        ----------
        cy : int
            center of y designated by pixel 
        cx : int
            center of y designated by pixel 
        r_in : int
            the inner radius of circle designated by pixel
        r_out : int
            the outer radius of circle designated by pixel
        azu_angles : 2Darray(float):
            designate inner and outer azimuthal angles
        dr : float

        dtheta : float(rad)

        Return
        ------
        c_filter : 2Darray(int(binary))
            position of filter

        """
        d_filter = np.zeros((self.ky, self.kx//self.bin))
        a_filter = np.zeros((self.ky, self.kx//self.bin))
        dy = dr*np.sin(dtheta)
        dx = dr*np.cos(dtheta)
        y_array, x_array, _,_ = self.make_darray(cy, cx)
        ye1_array, xe1_array, de1_array, _ = self.make_darray(cy+dy, cx+dx)
        ye2_array, xe2_array, de2_array, _ = self.make_darray(cy-dy, cx-dx)
        d_array = (de1_array+de2_array)/2
        if unit == 'rad':
            d_array *= self.pixelsize_k_rad
        if self.kx//self.bin/2 <= r_out:
            pass
            #print("r_out is too large.Should be smaller than ", self.kx//self.bin*self.pixelsize_k/2)
        a_array = np.arctan2(y_array*-1, x_array)*180 /np.pi
        ypos, xpos = np.where(a_array < 0)
        a_array[ypos, xpos] += 360
        d_filter[np.where((d_array >= r_in) & (d_array < r_out))] = 1
        for angle in range(azu_angles.shape[0]):
            a_filter[np.where((a_array >= azu_angles[angle][0])
                             &(a_array < azu_angles[angle][1]))] = 1
        c_filter = d_filter*a_filter
        return c_filter

    def make_dpcfilter(self, r_in, r_out):
        dpc = self.make_circularfilter(r_in,
                                       r_out,
                                       np.array([[0, 90],
                                                 [180, 270]]))
        dpc -= self.make_circularfilter(r_in,
                                        r_out,
                                        np.array([[90, 180],
                                                  [270, 360]]))
        return dpc


    def make_segmented_array(self, srad, erad, width, step, azu_angles, dr=0, dteta=0):
        filt_num = int((erad-srad)/step) + 1
        segmented_array = np.zeros((self.x, self.y, filt_num))
        for n in range(filt_num):
            segmented_array[:, :, n] = self.process_af_from_lines(srad+step*n,
                                                                  srad+step*n+width,
                                                                  azu_angles)
        return segmented_array

    def make_rotate_ellipse(self, r_in, r_out, azu_angles, dr, theta_step):
        cpu_num = os.cpu_count()//2
        filt_num = int(theta_step)
        segmented_array = np.zeros((self.y, self.x, filt_num))
        wrap = [[r_in, r_out, np.array([[0,360]]), 0, 1e6, dr, np.pi*n/filt_num] for n in range(filt_num)]
        wrap = np.array(wrap)
        with Pool(processes=cpu_num) as p:
            segmented_array = p.map(self.wrapper, wrap)
        return np.stack(segmented_array, axis=2)

    def wrapper(self, args):
        return self.process_af_from_lines(*args)

    def wrapper_ave(self, args):
        return self.process_ave_from_lines(*args)

    def wrapper_std(self, args):
        return self.process_std_from_lines(*args)

    def wrapper_radius(self, args):
        return self.process_radius_from_lines(*args)

    def make_segmented_array_multi(self, srad, erad, width, step, azu_angles):
        filt_num = int((erad-srad)/step) + 1
        cpu_num = os.cpu_count()//2
        wrap = [[srad+step*n, srad+step*n+width, np.array(azu_angles)] for n in range(filt_num)]
        wrap = np.array(wrap)
        with Pool(processes=cpu_num) as p:
            segmented_array = p.map(self.wrapper_ave, wrap)
        return np.stack(segmented_array, axis=2)

    def get_image(self, y, x):
        tifname = glob.glob(self.linename + "line_"+ str(y) + ".tif")
        lineimage = self.bin_lineimage(hs.load(tifname).data)
        image = lineimage[x, :, :]
        return image

    def make_fluctuation(self, imgfilter):
        mean = self.process_image_from_lines(imgfilter, 1)
        square = self.process_image_from_lines(imgfilter, 2)
        fluctuation = square/(mean**2)*mean.size-1
        Image.fromarray(fluctuation).save(self.processname + 'fluctuation.tif')
        return fluctuation

    def apply_multifilter(self, imgfilters, imgsname):
        tifnames = glob.glob(self.linename + "*.tif")
        num = imgfilters.shape[2]
        images = np.zeros((self.y, self.x, num))
        cnt = 0
        for name in tifnames:
            y = int(name[name.rfind('line_')+5:name.rfind('.tif')])
            if self.bin != 1:
                lineimage = self.bin_lineimage(hs.load(name).data)
            else:
                lineimage = hs.load(name).data
            images[y, :, :] = np.dot(lineimage, imgfilters).sum(1).sum(1)
        hs.signals.Signal2D(images.transpose(1,2,0).astype(np.float32)).save(self.processname+imgsname,
                                                         overwrite=True,
                                                         extension='tif')
        return images

    def apply_multifilter_multi(self, r_in, r_out, div_num, imgsname, thresh_min=0, thresh_max=np.inf, saveimage=True, filt=1):
        cpu_num = os.cpu_count()//2
        angle = 360/div_num
        wrap = [[r_in, r_out, np.array([[n*angle,(n+1)*angle]]), thresh_min, thresh_max, 0, 0, filt] for n in range(div_num)]
        wrap = np.array(wrap)
        with Pool(processes=cpu_num) as p:
            images = p.map(self.wrapper, wrap)
        images = np.stack(images, axis=2)
        print(div_num)
        if saveimage==True:
            if div_num == 1:
                hs.signals.Signal2D(images.reshape((self.y, self.x)).astype(np.float32))\
                                   .save(self.processname+imgsname,
                                         overwrite=True,
                                         extension='tif')
            if div_num > 1:
                hs.signals.Signal2D(images.transpose(2,0,1).astype(np.float32))\
                                   .save(self.processname+imgsname,
                                         overwrite=True,
                                         extension='tif')
        return images

    def apply_multifilter_multi_radius(self, r_in, r_out, div_num, imgsname, thresh_min=0, thresh_max=np.inf, saveimage=True, filt=1):
        cpu_num = os.cpu_count()//2
        angle = 360/div_num
        wrap = [[r_in, r_out, np.array([[n*angle,(n+1)*angle]]), thresh_min, thresh_max, 0, 0, filt] for n in range(div_num)]
        wrap = np.array(wrap)
        with Pool(processes=cpu_num) as p:
            images = p.map(self.wrapper_radius, wrap)
        images = np.stack(images, axis=2)
        if saveimage==True:
            hs.signals.Signal2D(images.transpose(2,0,1).astype(np.float32))\
                               .save(self.processname+imgsname,
                                     overwrite=True,
                                     extension='tif')
        return images

    def calc_std_multi(self, pixels, imgsname):
        cpu_num = os.cpu_count()//2
        div_num = self.ky//pixels//2
        wrap = [[pixels*n, pixels*(n+1), np.array([[0,360]])] for n in range(div_num)]
        wrap = np.array(wrap)
        with Pool(processes=cpu_num) as p:
            images = p.map(self.wrapper_std, wrap)
        images = np.stack(images, axis=2)
        with Pool(processes=cpu_num) as p:
            images_ave = p.map(self.wrapper_ave, wrap)
        images_ave = np.stack(images_ave, axis=2)
        images_norm = images/images_ave
        hs.signals.Signal2D(images.transpose(2,0,1).astype(np.float32))\
                           .save(self.processname+imgsname+'_std',
                                 overwrite=True,
                                 extension='tif')
        hs.signals.Signal2D(images_ave.transpose(2,0,1).astype(np.float32))\
                           .save(self.processname+imgsname+'_ave',
                                 overwrite=True,
                                 extension='tif')
        hs.signals.Signal2D(images_norm.transpose(2,0,1).astype(np.float32))\
                           .save(self.processname+imgsname+'_norm',
                                 overwrite=True,
                                 extension='tif')

    def filter4D(self, filter4d):
        self.smoothed_linename = self.foldername + 'Smoothed/'
        os.makedirs(self.smoothed_linename,
                    exist_ok=True)
        abs_image = np.zeros((self.y, 
                              self.x))
        image4d = np.zeros((self.y,
                            self.x,
                            self.ky,
                            self.kx//self.bin),
                            dtype = np.complex64)
        filter4d = filter4d.astype(np.complex64)
        tifnames = glob.glob(self.linename+'*.tif')
        cnt = 0
        for name in tifnames:
            print('read', cnt)
            y = int(name[name.rfind('line_')+5:name.rfind('.tif')])
            lineimage = np.array(hs.load(name).data)
            image4d[y, :, :, :] = self.bin_lineimage(lineimage)
            cnt += 1
        del(lineimage,tifnames)
        image4d[:, :, :, :] = np.fft.fft2(image4d[:, :, :, :], axes=(0, 1))
        image4d[:, :, :, :] = np.fft.fft2(image4d[:, :, :, :], axes=(2, 3))
        filter4d[:, :, :, :] = np.fft.fft2(filter4d[:, :, :, :], axes=(0, 1))
        filter4d[:, :, :, :] = np.fft.fft2(filter4d[:, :, :, :], axes=(2, 3))
        image4d *= filter4d
        image4d[:, :, :, :] = np.fft.ifft2(image4d[:, :, :, :], axes=(2, 3))
        image4d[:, :, :, :] = np.fft.ifft2(image4d[:, :, :, :], axes=(0, 1))
        image4d[:, :, :, :] = np.fft.fftshift(image4d[:, :, :, :], axes=(2, 3))
        image4d[:, :, :, :] = np.fft.fftshift(image4d[:, :, :, :], axes=(0, 1))
        for y in range(self.y):
            print(y)
            fft_images_y = hs.signals.Signal2D(image4d[y,:,:,:].astype(np.float32).transpose(0,1,2))
            fft_images_y.save(self.smoothed_linename + '/line_'+str(y),
                              overwrite=True,
                              extension='tif')

    def calc_angle_correlation(self, r_in, r_out, dr, da, thresh_min=0, thresh_max=np.inf, name=''):
        rnum = (r_out-r_in)//dr
        corrs = np.zeros((rnum, da))
        corr_pos = np.zeros((self.y, self.x, rnum, da))
        for r in range(rnum):
            images = self.apply_multifilter_multi(r_in+dr*r, r_in+dr*(r+1), da, 'None', thresh_min, thresh_max, False)
            images = images.transpose(2,0,1)
            corr_s = np.average(images, axis=0)**2
            for a in range(da):
                images_r = np.roll(images, a, axis=0)
                corr_r = np.average(images*images_r, axis=0)
                corr = corr_r/corr_s - 1
                corr_pos[:,:,r,a] = corr
                corrs[r, a] = np.average(corr)
        hs.signals.Signal2D(corrs.astype(np.float32)).save(self.processname+name,
                                                          overwrite=True,
                                                          extension='tif')
        np.save(self.processname+'angle_corrlation_pos', corr_pos.transpose(2,3,0,1).astype(np.float32))

    def calc_angle_radius(self, r_in, r_out, da, thresh_min=0, thresh_max=np.inf, name=''):
        _, _, dfilt, _ = self.make_darray(np.average(self.cy), np.average(self.cx))
        dfilt = np.sqrt(dfilt)
        images = self.apply_multifilter_multi_radius(r_in, r_out, da, 'None', thresh_min, thresh_max, False, dfilt)
        images = images.transpose(2,0,1)
        images *= images
        hs.signals.Signal2D(images.astype(np.float32)).save(self.processname+name,
                                                          overwrite=True,
                                                          extension='tif')

    def fit_ellipse2D(self, image, radii, angle, p0, bounds):
        rtheta = (radii, angle)
        popt,pcov = curve_fit(self.ellipse2D, rtheta, image,
                              p0 = p0,
                              bounds=bounds,
                              maxfev=1e4)
        print(popt)
        return popt

    def ellipse2D(self, rtheta, I0, major, minor, theta0, sigma, Ie):
        r, theta = rtheta
        I = I0*np.exp(-1*np.square(major*minor
                                   -np.sqrt((minor*r*np.cos((theta-theta0)/180*np.pi))**2
                                           +(major*r*np.sin((theta-theta0)/180*np.pi))**2))/sigma) + Ie
        return I

    def make_bg(self):
        mean = hs.load(self.processname+'mean.tif').data
        mean = mean/self.x/self.y
        back = np.zeros((self.ky, self.kx//self.bin))
        back1 = np.average(mean[10//self.bin:30//self.bin, 100//self.bin:160//self.bin], axis=0)
        back2 = np.average(mean[10//self.bin:30//self.bin, 90//self.bin:110//self.bin])
        backup = back1-back2
        back1 = np.average(mean[-30//self.bin:-10//self.bin, 100//self.bin:160//self.bin], axis=0)
        back2 = np.average(mean[-30//self.bin:-10//self.bin, 90//self.bin:110//self.bin])
        backdown = back1-back2
        back[:self.ky//2,100//self.bin:160//self.bin] = backup
        back[self.ky//2:,100//self.bin:160//self.bin] = backdown
        Image.fromarray(back).save(self.processname +'bg.tif')
        mean -= back
        Image.fromarray(mean).save(self.processname+'mean_nbg.tif')

    def fit_ellipse2D_full(self, r_thresh, p0, bounds, yr=np.array((0,0)), thresh=np.inf, power=1, ratio=0, rot=0):
        if yr[1] == 0:
            yr[1] == self.y
        mean = hs.load(self.processname+'mean.tif').data
        back = hs.load(self.processname+'bg.tif').data
        mean = mean/self.x/self.y
        popts = np.zeros((6, yr[1]-yr[0], self.x))
        cnt = 0
        for y in range(yr[0], yr[1]):
            name = self.linename + 'line_' + str(y) + '.tif'
            if self.bin != 1:
                lineimage = self.bin_lineimage(hs.load(name).data)
            else:
                lineimage = hs.load(name).data
            for x in range(self.x):
                _, _, d_array, a_array = self.make_darray(self.cy[y, x], self.cx[y, x], ratio, rot)
                index = np.where((d_array >= r_thresh)&(d_array <= self.kx/2))
                d_array = d_array.flatten()
                a_array = a_array.flatten()
                index = np.where((d_array >= r_thresh)&(d_array <= self.kx/2))
                image = lineimage[x, :, :]
                image -= back
                image[np.where(image < 0)] = 0
                image = np.power(image, power)
                #image = np.where(image > thresh, thresh, 0.0)
                #image = np.where(image >= np.average(image.flatten()[index]),1,0)
                image = image.flatten()
                popt = self.fit_ellipse2D(image[index], d_array[index], a_array[index], p0, bounds)
                if popt[1] < popt[2]:
                    print(y, x)
                    temp = popt[1]
                    popt[1] = popt[2]
                    popt[2] = temp
                    popt[3] += 90
                popt[3] = popt[3]%180
                popts[:, y-yr[0], x] = popt
            cnt += 1
            print(cnt,'/',self.y)
        return popts

    def wrapper_fit(self, args):
        return self.fit_ellipse2D_full(*args)

    def fit_ellipse2D_multi(self, r_thresh, p0, bounds, thresh = np.inf, power=1, ratio=0.0, rot=0.0, imgsname='fitellipse_fair', saveimage=True):
        cpu_num = os.cpu_count()//2
        if cpu_num >= self.y:
            cpu_num = self.y
        print(cpu_num)
        num = self.y/cpu_num
        wrap = [[r_thresh, p0, bounds, np.array((int(n*num), int((n+1)*num))), thresh, power, ratio, rot] for n in range(cpu_num)]
        wrap = np.array(wrap)
        with Pool(processes=cpu_num) as p:
            images = p.map(self.wrapper_fit, wrap)
        images = np.hstack(images)
        if saveimage==True:
            hs.signals.Signal2D(images.astype(np.float32))\
                               .save(self.processname+imgsname,
                                     overwrite=True,
                                     extension='tif')
        return images
