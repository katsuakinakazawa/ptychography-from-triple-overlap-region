'''
*********************************************************************
Ptychography code for SSB and WDD reconstructoin and aberration measurment.
*********************************************************************
The codes were originally developed by
Katsuaki Nakazawa(ICYS fellow at NIMS Japan)
e-mail:NAKAZAWA.Katsuaki@nims.go.jp
and debugged and modified by
Kazutaka Mitsuishi (NIMS Japan)
e-mail:MITSUISHI.Kazutaka@nims.go.jp
based on the following papers and books:

Pennycook, T. J. et al. Efficient phase contrast imaging in STEM using a pixelated detector.
Part 1: experimental demonstration at atomic resolution. Ultramicroscopy 151, 160-167,
doi:10.1016/j.ultramic.2014.09.013 (2015).
Yang, H. et al. Simultaneous atomic-resolution electron ptychography and Z-contrast imaging of
light and heavy elements in complex nanostructures. Nat Commun 7, 12532,
doi:10.1038/ncomms12532 (2016).
Rodenburg, J. & Maiden, A. in Springer handbook of microscopy
(eds P. W. Hawkes & John C. H. Spence) Ch. 17, (Springer Nature, 2019).
Rodenburg, J. M. Advances in Imaging and Electron Physics 87-184 (2008).

*********************************************************************
*Disclaimer*
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*********************************************************************
'''

import os
import glob
import math
import cmath
import copy
import stem4d as stem4d
import cv2
import numpy as np
import hyperspy.api as hs
from PIL import Image
from scipy import ndimage
from skimage.restoration import unwrap_phase
from concurrent.futures import ProcessPoolExecutor

class Ptychograph(stem4d.Image4d):
    def __init__(self, foldername, dose=np.inf):
        super().__init__(foldername)
        if foldername[-1] != '/':
            foldername += '/'
        self.foldername = foldername
        if dose == np.inf:
            self.dose = np.inf
            self.fft_linename=self.foldername+'FFTs/'
            self.linename=self.foldername+'Lines/'
            self.processname=self.foldername+'Process/'
            os.makedirs(self.processname, exist_ok=True)
        elif dose != np.inf:
            self.set_dose(dose)

    def irradiate_finite_dose(self, dose=np.inf, poisson=False):
        '''
        Make 4D-STEM data with finite dose condition

        Parameters
        ----------
        dose : the amount of dose
            each array contains probability 
        poisson : bool
            designate distribute the electron by poisson or not

        Returns
        -------
        p : 1Darray(float)
            return calibrated probability
        '''
        if dose != np.inf:
            self.set_dose(dose)
        os.makedirs(self.linename, exist_ok=True)
        for y in range(self.y):
            print(y, '/', self.y)
            name = self.foldername + "Lines/line_" + str(y)+ ".tif"
            lineimage = np.array(hs.load(name).data).astype(np.float64)
            finiteimage = np.zeros_like(lineimage)
            for x in range(self.x):
                G = lineimage[x, :, :].flatten()
                p = np.hstack((G, 1-np.sum(G)))
                p = self.calibrate_probability(p)
                image = np.zeros_like(p)
                if poisson == True:
                    size = np.random.poisson(dose, 1)
                else:
                    size = dose
                random = np.random.choice(np.arange(p.size), size=size, p=p)
                ind, num = np.unique(random, return_counts=True)
                image[ind] = num
                finiteimage[x, :, :] = image[:-1].reshape((self.ky, self.kx))
            finiteimage = hs.signals.Signal2D(finiteimage.astype(np.float32))
            finiteimage.save(self.linename+'line_'+str(y),
                             overwrite=True,
                             extension='tif')

    def set_dose(self, dose):
        '''
        modify the name of member variables according to dose.

        Parameters
        ----------
        dose : the amount of dose
            each array contains probability 
        '''
        if dose == np.inf:
            self.dose = np.inf
            self.fft_linename=self.foldername+'FFTs/'
            self.linename=self.foldername+'Lines/'
            self.processname=self.foldername+'Process/'
            os.makedirs(self.processname, exist_ok=True)
        else:
            self.dose = dose
            self.fft_linename=self.foldername+'FFTs_dose' + str(dose) + '/'
            self.linename=self.foldername+'Lines_dose' + str(dose) + '/'
            self.processname=self.foldername+'Process_dose' + str(dose) + '/'
            os.makedirs(self.processname, exist_ok=True)

    def calibrate_probability(self, p):
        """
        Calibrate to make the sum of the probability array p to 1.

        Parameters
        ----------
        p : 1Darray(float)
            each array contains probability 

        Returns
        -------
        p : 1Darray(float)
            return calibrated probability
        """
        while np.sum(p) != 1 or np.any(p < 0):
            p -= np.sum(p)/p.size
            p[p<0] = 0
            p *= 1/np.sum(p)
        return p

    def calc_abs_phase(self, image, wrapped=False):
        """
        Calc modulus and phase of the input image.

        Parameters
        ----------
        image : 2Darray(complex float)
            image to be decomposed into modulus and phase
        wrapped : bool
            designate unwrap the input image or not

        Returns
        -------
        abs_image : 2Darray(float)
            the modulus component of the input image
        phase_image : 2Darray(float)
            the phase component of the input image
        """
        abs_image = np.abs(image)
        if wrapped==True:
            phase_image=np.angle(image)
        else:
            phase_image = unwrap_phase(np.angle(image))
            phase_image = np.array(phase_image)
        return abs_image, phase_image

    def make_sum_image(self):
        """
        Calculate the sum of Q(K, R) along K or R and save the images.

        """
        abso = np.zeros((self.y, self.x))
        abso_k = np.zeros((self.ky, self.kx//self.bin))
        tifnames = glob.glob(self.fft_linename+'*.tif')
        for name in tifnames:
            y = int(name[name.rfind('line_')+5:name.rfind('.tif')])
            lineimage = np.abs(hs.load(name).data.transpose(1,2,0))
            abso_k += lineimage.sum(0)
            abso[y, :] = lineimage.sum(1).sum(1)
        Image.fromarray(abso_k).save(self.processname+'fft_abs_k.tif')
        Image.fromarray(abso).save(self.processname+'fft_abs.tif')

    def make_sum_image_real(self):
        """
        Calculate the sum of I(K, R) along K or R and save the images.

        """
        abso = np.zeros((self.y, self.x))
        abso_k = np.zeros((self.ky, self.kx//self.bin))
        tifnames = glob.glob(self.linename+'*.tif')
        for name in tifnames:
            y = int(name[name.rfind('line_')+5:name.rfind('.tif')])
            lineimage = np.abs(hs.load(name).data.transpose(1,2,0))
            abso_k += lineimage.sum(2)
            abso[y, :] = lineimage.sum(0).sum(0)
        Image.fromarray(abso_k).save(self.processname+'abs_k.tif')
        Image.fromarray(abso).save(self.processname+'abs.tif')

    def get_FFT_image(self, y, x, saveimg=True):
        """
        get FFT image at y and x

        Parameters
        ----------
        y : int
            the y position of the required image
        x : int
            the x position of the required image
        saveimg : bool
            designate save image or not

        Returns
        -------
        image : 2Darray(complex)
            FFT image
        """
        name = self.fft_linename + "line_" + str(y)+ ".tif"
        lineimage = hs.load(name).data.transpose(1,2,0).astype(np.complex)
        image = lineimage[x, :, :]
        if saveimg == True:
            abs_image, phase_image = self.calc_abs_phase(image, wrapped=True)
            Image.fromarray(abs_image).save(self.processname
                                           + 'FFT_abs_image_y='
                                           + str(y)
                                           + '_x='
                                           + str(x)
                                           + '.tif')
            Image.fromarray(phase_image).save(self.processname
                                             + 'FFT_phase_image_y='
                                             + str(y)
                                             + '_x='
                                             + str(x)
                                             + '.tif')
        return image

    def make_null_FFT4D(self):
        """
        FFT 4DSTEM data along y and x direction with null data

        Parameters
        ----------
        """
        os.makedirs(self.fft_linename, exist_ok=True)
        for y in range(self.y):
            fftname = self.fft_linename + '/line_'+str(y)
            fftimage = np.zeros((self.x, self.ky, self.kx//self.bin), dtype=np.complex64)
            fftimage = hs.signals.Signal2D(fftimage.transpose(2,0,1))
            fftimage.save(self.fft_linename + '/line_'+str(y),
                          overwrite=True,
                          extension='tif')


    def FFT4D(self, div = 2, start = 0):
        """
        FFT 4DSTEM data along y and x direction

        Parameters
        ----------
        div : int
            the number of division of this calculation
            to save memory large value must be used
        """
        image4d = np.zeros((self.y, self.x, self.ky//div, self.kx//self.bin),dtype = np.complex64)
        tifnames = glob.glob(self.linename+'*.tif')
        cnt = 0
        dnum = self.ky//div
        for d in range(start, div):
            for name in tifnames:
                print('read', cnt)
                y = int(name[name.rfind('line_')+5:name.rfind('.tif')])
                lineimage = np.array(hs.load(name).data)
                print(lineimage.shape)
                image4d[y, :, :, :] = self.bin_lineimage(lineimage)[:,d*dnum:(d+1)*dnum,:]
                cnt += 1
            image4d[:,
                    :,
                    :, 
                    :] = np.fft.fftshift(np.fft.fft2(image4d[:,
                                                             :,
                                                             :, 
                                                             :],
                                          axes=(0, 1)),
                                          axes=(0, 1)).astype(np.complex64)
            for y2 in range(self.y):
                fft_images_y = hs.load(self.fft_linename+'/line_'+str(y2)+'.tif').data.transpose(1,2,0)
                fft_images_y[:,d*dnum:(d+1)*dnum,:] = image4d[y2,:,:,:]
                fft_images_y = hs.signals.Signal2D(fft_images_y.transpose(2,0,1))
                fft_images_y.save(self.fft_linename + '/line_'+str(y2),
                                  overwrite=True,
                                  extension='tif')

    def FFT4D_mimic(self, repeat=1, div = 2):
        """
        FFT 4DSTEM data along y and x direction with virtual vaccum area

        Parameters
        ----------
        repeat : int
            designate the vaccum area
        div : int
            the number of division of this calculation
            to save memory large value must be used
        """
        self.y *= repeat
        self.x *= repeat
        image4d = np.zeros((self.y, self.x, self.ky//div, self.kx//self.bin),dtype = np.complex64)
        cnt = 0
        dnum = self.ky//div
        self.set_repeat(repeat)
        for d in range(div):
            for y in range(self.y):
                print(y)
                lineimage = np.zeros((self.x, self.ky, self.kx)).astype(np.complex64)
                if y < self.y//repeat:
                    name = self.linename + 'line_'+str(y) +'.tif'
                    lineimage[:self.x//repeat, :, :] = hs.load(name).data
                image4d[y, :, :, :] = self.bin_lineimage(lineimage)[:,d*dnum:(d+1)*dnum,:]
                cnt += 1
            image4d[:,
                    :,
                    :, 
                    :] = np.fft.fftshift(np.fft.fft2(image4d[:,
                                                             :,
                                                             :, 
                                                             :],
                                          axes=(0, 1)),
                                          axes=(0, 1)).astype(np.complex64)
            for y2 in range(self.y):
                fft_images_y = hs.load(self.fft_linename+'/line_'+str(y2)+'.tif').data.transpose(1,2,0)
                fft_images_y[:,d*dnum:(d+1)*dnum,:] = image4d[y2,:,:,:]
                fft_images_y = hs.signals.Signal2D(fft_images_y.transpose(2,0,1))
                fft_images_y.save(self.fft_linename + '/line_'+str(y2),
                                  overwrite=True,
                                  extension='tif')

    def measure_center_and_radius(self, y=None, x=None):
        """
        measure center of reciprocal space
        measure radius of transmitted beam

        Parameters
        ----------
        y : int
            the y position to measure center and radius
        x : int
            the x position to measure center and radius

        Returns
        -------
        cen_ky : float
            the center in ky axis of the transmitted beam
        cen_kx : float
            the center in kx axis of the transmitted beam
        radius : float
            the radius of transmitted beam
        """
        if y == None:
            y = self.y//2
        if x == None:
            x = self.x//2
        image = self.get_FFT_image(y, x)
        thresh = np.zeros((self.ky, self.kx//self.bin))
        thresh[np.where(image >= np.mean(image))] = 1
        Image.fromarray(thresh).save(self.processname + 'thresh_image.tif')
        cen_ky, cen_kx = ndimage.measurements.center_of_mass(thresh)
        radius = math.sqrt(np.sum(thresh)/np.pi) * self.pixelsize_k_rad
        self.cen_kx = cen_kx
        self.cen_ky = cen_ky
        self.radius = radius
        print("cen_ky=",cen_ky," cen_kx=",cen_kx,"radius=",radius)
        return cen_ky, cen_kx, radius

    def calc_distanceratio_and_rotation(self,
                                        diff_ky,
                                        diff_kx,
                                        diff_y,
                                        diff_x,
                                        cen_ky=None,
                                        cen_kx=None,
                                        cen_y=None,
                                        cen_x=None):
        """
        calc distance ratio between real and reciprocal space
        calc difference of direction of shift of overlap region
        set the values as member variables of this class

        Parameters
        ----------
        diff_ky : float
            the center in ky axis of the diffracted beam
        diff_kx : float
            the center in kx axis of the diffracted beam
        diff_y : float
            the position of y which was used to measure diff_ky & diff_kx
        diff_y : float
            the position of x which was used to measure diff_ky & diff_kx
        cen_ky : float
            the center in ky axis of the transmitted beam
        cen_kx : float
            the center in kx axis of the transmitted beam
        cen_y : float
            the center in y axis of scanning position
        cen_x : float
            the center in x axis of scanning position

        Returns
        -------
        distance_ratio : float
            calculated distance ratio(reciprocal/real)
        rotation : float(degree)
            calculated rotation(reciprocal-real)
        """
        if cen_ky == None:
           cen_ky = self.cen_ky
        if cen_kx == None:
           cen_kx = self.cen_kx
        if cen_y == None:
           cen_y = self.y//2
        if cen_x == None:
           cen_x = self.x//2
        diff1 = math.sqrt((cen_y-diff_y)**2 + (diff_x-cen_x)**2)
        diff2 = math.sqrt((cen_ky-diff_ky)**2 + (diff_kx-cen_kx)**2)
        distance_ratio = diff2/diff1
        angle1 = math.atan2(cen_y - diff_y, diff_x-cen_x)
        angle2 = math.atan2(cen_ky - diff_ky, diff_kx-cen_kx)
        rotation = angle2 - angle1
        self.distance_ratio = distance_ratio
        self.rotation = rotation
        return distance_ratio, rotation

    def calc_offsets(self, y, x):
        """
        calc shift of overlap region in reciprocal space 
        from y and x value in real space

        Parameters
        ----------
        y : int
            the position of y
        x : int
            the position of x

        Returns
        -------
        offset_ky : int
            the shift along ky axis
        offset_kx : int
            the shift along kx axis
        """
        cen_y = self.y//2
        cen_x = self.x//2
        distance = math.sqrt((cen_y - y)**2 + (x - cen_x)**2)*self.distance_ratio
        theta = math.atan2(cen_y - y, x - cen_x) + self.rotation
        offset_ky = -1 * distance * math.sin(theta)
        offset_kx = distance * math.cos(theta)
        return offset_ky, offset_kx

    def WDD(self, aberrations=np.zeros(12), filename=""):
        """
        Restore phase information by WDD method
        Save Restored abso and phase images

        Parameters
        ----------
        aberration : 2Darray(float)
            aberration to be removed in restoration
        filename : str
            name for the restored images

        Returns
        -------
        aberration : 2Darray(float)
            calculated aberration values
        """
        phi_k = np.zeros((self.y, self.x)).astype(np.complex)
        abe_image = self.make_aberration_image(0, 0, aberrations)
        for qy in range(self.y):
            name = self.fft_linename + "/line_" + str(qy)+ ".tif"
            lineimage = np.array(hs.load(name).data.transpose(1,2,0).astype(np.complex))
            for qx in range(self.x):
                G = lineimage[qx, :, :]
                offset_ky, offset_kx = self.calc_offsets(qy, qx)
                H = np.fft.ifft2(np.fft.ifftshift(G))
                aperture_zero = self.make_circularfilter(self.cen_ky,
                                                         self.cen_kx,
                                                         0,
                                                         self.radius,
                                                         unit='rad')
                aperture_plus_k = self.make_circularfilter(self.cen_ky + offset_ky,
                                                            self.cen_kx + offset_kx,
                                                            0,
                                                            self.radius,
                                                            unit='rad')
                probe_zero = abe_image * aperture_zero
                probe_plus_k = np.roll(abe_image,
                                       (round(offset_ky), round(offset_kx)),
                                       axis=(0, 1))
                probe_plus_k *= aperture_plus_k
                A = probe_zero * np.conj(probe_plus_k)
                if np.sum(A) != 0:
                    kai_A = np.fft.ifft2(np.fft.ifftshift(A))
                    kai_phi = np.conj(kai_A)*H/(np.abs(kai_A)**2+0.1)
                    D = np.fft.fftshift(np.fft.fft2(kai_phi))
                    phi_k[qy, qx] = D[self.ky//2, self.kx//2//self.bin]
                if qy == self.y//2 and qx == self.x//2:
                    D0 = np.sqrt(D[self.ky//2, self.kx//2//self.bin])
        phi = np.conj(phi_k)/D0
        phi = np.fft.ifft2(np.fft.ifftshift(phi))
        phi = np.flip(phi)
        abs_phi, phase_phi = self.calc_abs_phase(phi)
        Image.fromarray(abs_phi).save(self.processname + 'Phi_abs_WDD_'+ filename + '.tif')
        Image.fromarray(phase_phi).save(self.processname  +'Phi_phase_WDD_'+ filename + '.tif')

    def calc_aberrations(self, y, x):
        """
        calc aberrations with designated pixel
        from defocus to 3rd order aberration

        Parameters
        ----------
        y : int
            the position of y
        x : int
            the position of x

        Returns
        -------
        aberration : 2Darray(float)
            calculated aberration values
        """
        y *= self.pixelsize_k_rad
        x *= self.pixelsize_k_rad
        aberration = np.array([(x**2 + y**2)/2,
                               (x**2 - y**2)/2,
                               x*y,
                               (x**3 - 3*x*y**2)/3,
                               (3*x**2*y - y**3)/3,
                               (x**3 + x*y**2)/3,
                               (y**3 + x**2*y)/3,
                               (x**4 + y**4 + 2*x**2*y**2)/4,
                               (x**4 + y**4 - 6*x**2*y**2)/4,
                               x**3*y - x*y**3,
                               (x**4 - y**4)/4,
                               (x**3*y + x*y**3)/2])
        aberration *= 2*math.pi/(self.wavelength)
        return aberration

    def make_Gs(self, Qs):
        """
        Parameters
        ----------
        Qs : 2Darray(int)
            the positions of Qs for measurement of aberration

        Returns
        -------
        Gs : 2Darray(complex float)
            the modulus and phase distribution at Qs
        """
        num = len(Qs)
        Gs = np.zeros((num, self.ky, self.kx//self.bin)).astype(np.complex128)
        for n in range(num):
            Gs[n,:,:]  = self.get_FFT_image(Qs[n][0], Qs[n][1])
        return Gs

    def measure_aberration(self,
                           Qs,
                           Gs,
                           region='triple',
                           mode='iterative',
                           update_thresh=0.001):
        """
        measure aberration from overlap regions

        Parameters
        ----------
        Qs : 2Darray(int)
            the positions of Qs for measurement of aberration
        Gs : 2Darray(complex float)
            the modulus and phase distribution at Qs
        region : str
            designate method for the measurement of aberration
            [double, triple, both, abso, full]
        mode : str
            designate mode for the measurment of aberration
            [inverse, iterative]
        iteration : int
            maximum number for the iteration.
            effective in iterative mode
        """
        num = len(Qs)
        abe_mat = np.zeros((0,12))
        b_array = np.zeros(0)
        abs_obj = np.zeros((num))
        abe_array = np.zeros((12))
        beta = np.zeros(12)
        if region in ['both', 'full']:
            identity_part = np.zeros((0, 2*num))
        else:
            identity_part = np.zeros((0, num))
        Q_K_list_d = list() 
        Q_K_list_t = list() 
        for i in range(num):
            y = Qs[i][0]
            x = Qs[i][1]
            print(y, x)
            G = Gs[i,:,:]
            offset_ky, offset_kx = self.calc_offsets(y, x)
            overlap_d = self.make_overlap_region_with_aberration(offset_ky,
                                                                 offset_kx,
                                                                 np.zeros(12),
                                                                 'minus')[1]
            overlap_d = cv2.erode(overlap_d.astype(np.uint8),kernel=np.ones((3,3), np.uint8))
            overlap_t = self.make_overlap_region_with_aberration(offset_ky,
                                                                 offset_kx,
                                                                 np.zeros(12),
                                                                 'center')[1]
            overlap_t = cv2.erode(overlap_t.astype(np.uint8),kernel=np.ones((3,3), np.uint8))
            overlap_full = self.make_overlap_region_with_aberration(offset_ky,
                                                                    offset_kx,
                                                                    np.zeros(12),
                                                                    'full')[1]
            abs_obj[i] = np.average(np.abs(G[np.where(np.abs(overlap_d) > 0.5)]))
            kys_d, kxs_d = np.where(np.abs(overlap_d) != 0)
            kys_t, kxs_t = np.where(np.abs(overlap_t) >= 1e-6)
            abs_G, phase_G = self.calc_abs_phase(G)
            pixels_d = len(kys_d)
            pixels_t = len(kys_t)
            pixels = pixels_d + pixels_t
            if region == 'double':
                pixels = pixels_d
                pixels_t = 0
                thresh = pixels
            elif region == 'triple' or region == 'abso':
                pixels = pixels_t
                pixels_d = 0
                thresh = pixels
            elif region == 'both' or region == 'full':
                thresh = pixels
            print("len(kys)=", pixels)
            if thresh > 300:
                thresh = 300
                print(f"If the len(kys) is more than {thresh}, it trimed down to {thresh}")
            k_inds_d = np.random.choice(np.arange(len(kys_d)),
                                                  int(thresh*pixels_d/pixels),
                                                  replace=False)
            k_inds_t = np.random.choice(np.arange(len(kys_t)),
                                                  int(thresh*pixels_t/pixels),
                                                  replace=False)
            kys_d = kys_d[k_inds_d]
            kxs_d = kxs_d[k_inds_d]
            kys_t = kys_t[k_inds_t]
            kxs_t = kxs_t[k_inds_t]
            Q_K_list_d.append([y, x, kys_d, kxs_d])
            Q_K_list_t.append([y, x, kys_t, kxs_t])
            if region in ['double', 'triple', 'both','full']:
                abe_mat = np.vstack((abe_mat, 
                                     self.make_abe_matrix(offset_ky,
                                                          offset_kx,
                                                          kys_d,
                                                          kxs_d,
                                                          'double')))
                b_array = np.hstack((b_array, 
                                     self.make_b_array(G, kys_d, kxs_d, 'double_phase')))
                b_abso = abs_G[kys_t, kxs_t]
                kys_t = kys_t[b_abso >= 1e-6]
                kxs_t = kxs_t[b_abso >= 1e-6]
                abe_mat_t = self.make_abe_matrix(offset_ky,
                                                 offset_kx,
                                                 kys_t,
                                                 kxs_t,
                                                 'triple_phase')
                abe_mat = np.vstack((abe_mat, abe_mat_t))
                b_temp = self.make_b_array(G*overlap_t, kys_t, kxs_t, 'triple_phase')
                b_array = np.hstack((b_array, b_temp+np.pi/2))
            if region in ['abso', 'full']:
                abe_mat = np.vstack((abe_mat, 
                                     self.make_abe_matrix(offset_ky,
                                                          offset_kx,
                                                          kys_t,
                                                          kxs_t,
                                                          'triple_abso')))
                G_obj = np.abs(G/abs_obj[i]/2)
                G_obj[np.where(G_obj > 1)] = 1
                b_array = np.hstack((b_array, 
                                     self.make_b_array(G_obj, 
                                                       kys_t, 
                                                       kxs_t,
                                                       mode='abso')))
            if region == 'abso':
                identity_part_tmp = np.zeros((len(kys_t), num))
            elif region == 'full':
                identity_part_tmp = np.zeros((len(kys_d)+2*len(kys_t), 2*num))
                identity_part_tmp[:len(kys_d),i*2] = 1 
                identity_part_tmp[len(kys_d):len(kys_d)+len(kxs_t),i*2+1] = 1 
            elif region == 'both':
                identity_part_tmp = np.zeros((len(kys_d)+len(kys_t), 2*num))
                identity_part_tmp[:len(kys_d),i*2] = 1
                identity_part_tmp[len(kys_d):len(kys_d)+len(kys_t),i*2+1] = 1
            else:
                identity_part_tmp = np.zeros((len(kys_d)+len(kys_t), num))
                identity_part_tmp[:,i] = 1
            identity_part = np.vstack((identity_part, identity_part_tmp))
        b_num = b_array.size
        if region != 'abso':
            abe_mat = np.hstack((abe_mat, identity_part))
        abe_mat_inv = self.make_inv_svd(abe_mat)
        if mode == 'inverse':
            abe_array = abe_mat_inv.dot(b_array.T)[:12]
            phase = abe_mat_inv.dot(b_array.T)[12:]
        elif mode == 'iterative':
            abe_array = np.zeros(12)
            for c in range(1,13):
                beta[:c] = 0.5
                update_ratio = np.inf
                abe_array_dif = np.ones(12+num)
                cnt = 0
                while update_ratio >= 0.001 and cnt < 100:
                    abe_array_dif = abe_mat_inv.dot(b_array.T)
                    update_ratio = abs(abe_array_dif[c-1]/abe_array[c-1])
                    abe_array += abe_array_dif[:12]*beta 
                    print(abe_array_dif)
                    print(abe_array)
                    print(update_ratio)
                    print(cnt)
                    b_array = np.zeros(b_num)
                    b_pos = 0
                    for n in range(num):
                        y, x, kys_d, kxs_d = Q_K_list_d[n]
                        y, x, kys_t, kxs_t = Q_K_list_t[n]
                        G = Gs[n]
                        offset_ky, offset_kx = self.calc_offsets(y, x)
                        overlap = self.make_overlap_region_with_aberration(offset_ky,
                                                                           offset_kx,
                                                                           abe_array,
                                                                           'full')[0]
                        G_diff = G/overlap
                        G_diff[overlap==0] = 0
                        G_diff_abs, G_diff_phase = self.calc_abs_phase(G_diff, wrapped=False)
                        if region != 'abso':
                            b_array[b_pos:b_pos+len(kys_d)] = self.make_b_array(G_diff,
                                                                                kys_d,
                                                                                kxs_d,
                                                                                'double_phase')
                            b_pos += len(kys_d)
                            b_temp = self.make_b_array(G_diff,
                                                       kys_t,
                                                       kxs_t,
                                                       'triple_phase')
                            b_array[b_pos:b_pos+len(kys_t)] = b_temp
                            b_pos += len(kys_t)
                        if region == 'abso' or region == 'full':
                            G_diff = np.abs((np.abs(G)/abs_obj[i]/2-np.abs(overlap)/2))
                            G_diff[np.where(G_diff > 1)] = 1
                            G_diff[np.where(G_diff < 0)] = 0
                            b_array[b_pos:b_pos+len(kys_t)] = self.make_b_array(G_diff,
                                                                                kys_t,
                                                                                kxs_t,
                                                                                'abso')
                            b_pos += len(kys_t)
                    cnt += 1
        np.save(f'{self.processname}aberration_{region}_{mode}.npy', abe_array)
        return abe_array

    def test_measure_aberration(self, aberrations, Qs, region='triple', mode='iterative'):
        """
        measure aberration from overlap regions

        Parameters
        ----------
        Qs : 2Darray(int)
            the positions of Qs for measurement of aberration
        region : str
            designate method for the measurement of aberration
            [double, triple, both, abso, full]
        mode : str
            designate mode for the measurment of aberration
            [inverse, iterative]
        iteration : int
            maximum number for the iteration.
            effective in iterative mode
        """
        num = len(Qs)
        Gs = np.zeros((num, self.ky, self.kx//self.bin)).astype(np.complex128)
        for i in range(num):
            y = Qs[i][0]
            x = Qs[i][1]
            offset_ky, offset_kx = self.calc_offsets(y, x)
            G = self.make_overlap_region_with_aberration(offset_ky,
                                                         offset_kx,
                                                         aberrations,
                                                         'full')[0]
            Gs[i,:,:] = G*np.exp(1j*np.pi/2)
        abe_measured = self.measure_aberration(Qs, Gs, region=region, mode=mode)
        return abe_measured

    def make_overlap_region_with_aberration(self, dky, dkx, abe_array, area='both'):
        """
        make overlap region with aberration

        Parameters
        ----------
        dky : float
            the value for shift in ky direction
        dkx : float
            the value for shift in kx direction
        abe_array  : 2Darray(float)
            matrix to calculate inverse matrix
        area  : str
            designate overlap region to calculate
            [plus, minus, both, full]

        Returns
        -------
        overlap : 2Darray(complex float)
            overlap region with aberration
        app : 2Darray(float)
            the area of overlap region
        """
        disk_zero = self.make_disk_with_aberration(0, 0, abe_array)
        app_zero = np.where(np.abs(disk_zero)>0.5, 1, 0).astype(np.complex)
        disk_plus = self.make_disk_with_aberration(dky,
                                                   dkx,
                                                   abe_array)
        app_plus = np.where(np.abs(disk_plus)>0.5, 1, 0).astype(np.complex)
        disk_minus = self.make_disk_with_aberration(-dky,
                                                    -dkx,
                                                    abe_array)
        app_minus = np.where(np.abs(disk_minus)>0.5, 1, 0).astype(np.complex)
        overlap = np.conj(disk_zero)*disk_minus - disk_zero*np.conj(disk_plus)
        app = np.zeros_like(app_zero)
        if area in ['plus', 'both', 'full']:
            app += app_plus * app_zero * np.abs(app_minus-1)
        if area in ['minus', 'both', 'full']:
            app += app_minus * app_zero * np.abs(app_plus-1)
        if area in ['center', 'full']:
            app += app_zero * app_plus * app_minus
        overlap *= app
        return overlap, app

    def make_disk_with_aberration(self, dky, dkx, abe_array, saveimg=False):
        """
        Make Probe image with aberration in reciprocal space.

        Parameters
        ----------
        dky : float
            the value for shift in ky direction
        dkx : float
            the value for shift in kx direction
        abe_array  : 2Darray(float)
            matrix to calculate inverse matrix
        saveimg  : bool
            designate save aberration image or not

        Returns
        -------
        abe : 2Darray(complex float)
            aberration image
        """
        abe = self.make_aberration_image(dky, dkx, abe_array, saveimg=saveimg)
        aperture = self.make_circularfilter(self.cen_ky + dky,
                                            self.cen_kx + dkx,
                                            0,
                                            self.radius,
                                            unit = 'rad')
        disk = aperture * abe
        return disk

    def make_aberration_image(self, dky, dkx, abe_array, saveimg=False):
        """
        Cake aberration distribution

        Parameters
        ----------
        dky : float
            the value for shift in ky direction
        dkx : float
            the value for shift in kx direction
        abe_array  : 2Darray(float)
            matrix to calculate inverse matrix
        saveimg  : bool
            designate save aberration image or not

        Returns
        -------
        abe : 2Darray(complex float)
            aberration image
        """
        abe_phase = np.zeros((self.ky, self.kx//self.bin))
        y, x, _, _ = self.make_darray(dky+self.cen_ky, dkx+self.cen_kx)
        y *= self.pixelsize_k_rad
        x *= self.pixelsize_k_rad
        abe_phase = (x**2 + y**2)/2*abe_array[0]\
                  + (x**2 - y**2)/2*abe_array[1]\
                  + (x*y)*abe_array[2]\
                  + (x**3 - 3*x*y**2)/3*abe_array[3]\
                  + (3*x**2*y - y**3)/3*abe_array[4]\
                  + (x**3 + x*y**2)/3*abe_array[5]\
                  + (y**3 + x**2*y)/3*abe_array[6]\
                  + (x**4 + y**4 + 2*x**2*y**2)/4*abe_array[7]\
                  + (x**4 + y**4 - 6*x**2*y**2)/4*abe_array[8]\
                  + (x**3*y - x*y**3)*abe_array[9]\
                  + (x**4 - y**4)/4*abe_array[10]\
                  + (x**3*y + x*y**3)/2*abe_array[11]
        abe_phase *= 2*math.pi/(self.wavelength)
        abe = np.exp(1j * abe_phase)
        if saveimg == True:
            abs_abe, phase_abe = self.calc_abs_phase(abe)
            Image.fromarray(abs_abe).save(self.processname + 'abe_abs.tif')
            Image.fromarray(phase_abe).save(self.processname + 'abe_phase.tif')
        return abe

    def make_inv_svd(self, matrix):
        """
        make inverse matrix with single value decomposition

        Parameters
        ----------
        matrix  : 2Darray(float)
            matrix to calculate inverse matrix

        Returns
        -------
        matrix_inv : 2Darray(float)
            inversed matrix
        """
        y, x = matrix.shape
        s, v, d = np.linalg.svd(matrix)
        if y > x:
            v = np.hstack((np.diag(1/v), np.zeros((x, y-x))))
        elif y < x:
            v = np.vstack((np.diag(1/v), np.zeros((x-y, y))))
        elif y == x:
            v = np.diag(1/v)
        matrix_inv = d.T.dot(v).dot(s.T)
        return matrix_inv

    def make_b_array(self, G, kys, kxs, mode='phase'):
        """
        make b array which contain phase or mudulus value

        Parameters
        ----------
        G  : 2Darray(complex float)
            image after FFT
        kys : 2Darray(float)
            the ky positions to pick up aberration
        kxs : 2Darray(float)
            the kx positions to pick up aberration
        mode : str
            designate mode for the measure aberration
            [double_phase, triple_phase, abso]

        Returns
        -------
        b_array : 2Darray(float)
            phase information of each spatial frequency
        """
        abso, phase = self.calc_abs_phase(G, wrapped=False)
        phase_triple = copy.copy(phase)
        phase_triple = self.unwrap_phase(abso, phase_triple)
        phase_triple = self.unwrap_phase(abso, phase_triple)
        b_array = np.zeros((kys.size))
        ind = 0
        for ky, kx in zip(kys, kxs):
            if mode == 'double_phase':
                b_array[ind] = phase[ky, kx]
            elif mode == 'triple_phase':
                b_array[ind] = phase_triple[ky, kx]
            elif mode == 'abso':
                b_array[ind] = np.arcsin(abso[ky, kx])
            ind += 1
        return b_array

    def unwrap_phase(self, abso, phase, cycle=np.pi):
        """
        unwrap the phase

        Parameters
        ----------
        abso  : 2Darray(float)
            Image of Modulus
        phase : 2Darray(float)
            Image of Phase
        cycle : float
            Criteria for unwrapping 
            when the difference between adjacent pixels exceeds this value

        Returns
        -------
        phase : 2Darray(float)
            phase information after unwrapped
        """
        h, w = phase.shape
        phase *= np.where(abso > 0, 1, 0)
        for i in range(4):
            for y in range(1,h-1):
                for x in range(1,w-1):
                    if abso[y, x] == 0:
                        continue
                    #elif abso[y-1, x] == 0 and abso[y,x-1] ==0:
                    #    continue
                    else:
                        if abso[y, x+1] != 0:
                            num = np.round((phase[y, x+1]-phase[y, x])/(cycle))
                            phase[y, x+1] -= num*cycle
                        elif x < w-2 and abso[y, x+2] != 0:
                            num = np.round((phase[y, x+2]-phase[y, x])/(cycle))
                            phase[y, x+2] -= num*cycle
                        if abso[y+1, x] != 0:
                            num = np.round((phase[y+1, x]-phase[y, x])/(cycle))
                            phase[y+1, x] -= num*cycle
                        elif y < h-2 and abso[y+2, x] != 0:
                            num = np.round((phase[y+2, x]-phase[y, x])/(cycle))
                            phase[y+2, x] -= num*cycle
            if i%2 == 1:
                phase = np.rot90(phase, 2)
                abso = np.rot90(abso, 2)
            if i%2 == 0:
                phase = np.rot90(phase)
                abso = np.rot90(abso)
        phase = np.rot90(phase, 2)
        abso = np.rot90(abso, 2)
        return phase

    def make_abe_matrix(self, dky, dkx, kys, kxs, mode):
        """
        make aberration matrix

        Parameters
        ----------
        dky : float
            the value for shift in ky direction
        dkx : float
            the value for shift in kx direction
        kys : 2Darray(float)
            the ky positions to pick up aberration
        kxs : 2Darray(float)
            the kx positions to pick up aberration
        mode : str
            designate mode for the measure aberration
            [double, triple_phase, triple_abso]

        Returns
        -------
        phi : 2Darray(complex float)
            phase information of each spatial frequency
        """
        abe_mat_sizem = kys.size
        abe_mat_sizen = 12
        abe_mat = np.zeros((abe_mat_sizem, abe_mat_sizen))
        ind = 0
        for ky, kx in zip(kys, kxs):
            if mode == 'double':
                abe_mat[ind, :] = self.calc_aberrations((ky+dky-self.cen_ky),
                                                        (kx+dkx-self.cen_kx))
                abe_mat[ind, :] -= self.calc_aberrations((ky-self.cen_ky),
                                                         (kx-self.cen_kx))
            elif mode == 'triple_phase':
                abe_mat[ind, :] = self.calc_aberrations((ky+dky-self.cen_ky),
                                                        (kx+dkx-self.cen_kx))
                abe_mat[ind, :] -= self.calc_aberrations((ky-dky-self.cen_ky),
                                                         (kx-dkx-self.cen_kx))
                abe_mat[ind, :] /= 2
            elif mode == 'triple_abso':
                abe_mat[ind, :] -= 2*self.calc_aberrations((ky-self.cen_ky),
                                                          (kx-self.cen_kx))
                abe_mat[ind, :] += self.calc_aberrations((ky-dky-self.cen_ky),
                                                         (kx-dkx-self.cen_kx))
                abe_mat[ind, :] += self.calc_aberrations((ky+dky-self.cen_ky),
                                                         (kx+dkx-self.cen_kx))
                abe_mat[ind, :] /= 2
            ind += 1
        return abe_mat

    def SSB(self, 
            aberrations=np.zeros(12), 
            filename="",
            mode='both',
            thresh=0.1,
            normalize=False,
            qys=np.array((0,0))):
        """
        Restore phase with Single side band method.

        Parameters
        ----------
        aberrations : 2Darray(float)
            the value for each aberrations
        filename : str
            filename for saving
        mode : str
            designate mode for the restoration
            [double, triple, both]
        thresh : float
            designate the value for restoration
        normalize : bool
            designate normalize the result or not
        qys : 2Darray(int)
            designate the rows to restore phase

        Returns
        -------
        phi : 2Darray(complex float)
            phase information of each spatial frequency
        """
        modes = np.array(['double', 'triple', 'both'])
        print(aberrations)
        if np.all(mode != modes):
            print(mode+' is not right. Choose double triple or both')
            raise ValueError
        if qys[1] == 0:
            qys[1] = self.y
        ys = qys[1]-qys[0]
        phi = np.zeros((ys, self.x)).astype(np.complex)
        radius = copy.copy(self.radius)
        for qy in range(qys[0], qys[1]):
            name = self.fft_linename + "line_" + str(qy)+ ".tif"
            print(qy, qy-qys[0]+1, '/', qys[1]-qys[0])
            lineimage = hs.load(name).data.transpose(1,2,0).astype(np.complex)
            for qx in range(self.x):
                area = 0
                amp = 0
                offset_ky, offset_kx = self.calc_offsets(qy, qx)
                offset = np.sqrt(offset_ky**2 + offset_kx**2)
                if mode == 'triple' and offset > radius/self.pixelsize_k_rad:
                    continue
                if mode in ['double', 'both'] and offset > 2 * radius/self.pixelsize_k_rad:
                    continue
                G = lineimage[qx, :, :]
                probe_double, A_double = self.make_overlap_region_with_aberration(offset_ky,
                                                                                  offset_kx,
                                                                                  aberrations,
                                                                                  'both')
                probe_triple, A_triple = self.make_overlap_region_with_aberration(offset_ky,
                                                                                  offset_kx,
                                                                                  aberrations,
                                                                                  'center')
                AP = np.zeros_like(G)
                self.radius = radius*(0.8+0.2*offset/(radius/self.pixelsize_k_rad))
                _, A_triple_core = self.make_overlap_region_with_aberration(offset_ky,
                                                                            offset_kx,
                                                                            aberrations,
                                                                            'center')
                A_triple_core = A_triple_core.astype(np.complex)
                self.radius = radius
                if np.sum(A_triple) != 0 and mode in ['triple', 'both']:
                    if np.average(np.abs(probe_triple[np.where(A_triple_core != 0)])) < thresh:
                        pass
                    else:
                        AP_triple = probe_triple/(np.abs(probe_triple)**2)
                        AP_triple[np.isnan(AP_triple)] = 0
                        AP += (AP_triple*A_triple_core)
                        area += np.sum(np.where(A_triple>0, 1, 0))
                if np.sum(np.abs(A_double)) != 0 and mode in ['double', 'both']:
                    AP += probe_double
                    area += np.sum(np.where(A_double>0, 1, 0))
                if np.sum(np.abs(AP)) != 0:
                    amp = np.average((G*AP)[np.where(AP != 0)])
                if qy == self.y//2 and qx == self.x//2:
                    AP = np.zeros_like(G)
                    AP = A_triple
                    amp = np.average((G*AP)[AP != 0])
                    area = np.sum(AP)
                phi[qy-qys[0], qx] = amp*area
                if normalize == True:
                    norm = np.sqrt(np.sum(np.where(np.conj(AP))[0]))
                    norm = np.max((1, norm))
                    phi[qy, qx] /= np.sqrt(np.sum(np.where(np.conj(AP))[0]))
                    phi[np.isnan(phi)] = 0
        phi[np.isnan(phi)] = 0
        abs_phi, phase_phi = self.calc_abs_phase(phi,wrapped=True)
        Image.fromarray(abs_phi).save(f'{self.processname}abs_SSB_{mode}{qys[0]}.tif')
        Image.fromarray(phase_phi).save(f'{self.processname}phase_SSB_{mode}{qys[0]}.tif')
        #return phi

    def SSB_multi(self, 
                  aberrations=np.zeros(12), 
                  filename="",
                  mode='both',
                  thresh=0.1,
                  normalize=False,
                  cpu_num=os.cpu_count()//2):
        """
        Restore phase with Single side band method.
        Save restored absolute and phase images.

        Parameters
        ----------
        aberrations : 2Darray(float)
            the value for each aberrations
        filename : str
            filename for saving
        mode : str
            designate mode for the restoration
            [double, triple, both]
        thresh : float
            designate the value for restoration
        normalize : bool
            designate normalize the result or not
        qys : 2Darray(int)
            designate the rows to restore phase
        """
        phi_phase = np.zeros((self.y, self.x)).astype(np.float32)
        phi_abs = np.zeros((self.y, self.x)).astype(np.float32)
        futures = []
        kwargs_list = []
        if mode == 'triple':
            st = self.y//2 - int(self.radius/self.pixelsize_k_rad/self.distance_ratio) - 1
            ed = self.y//2 + int(self.radius/self.pixelsize_k_rad/self.distance_ratio) + 1
        elif mode in ['both', 'double']:
            st = self.y//2 - 2*int(self.radius/self.pixelsize_k_rad/self.distance_ratio) - 1 
            ed = self.y//2 + 2*int(self.radius/self.pixelsize_k_rad/self.distance_ratio) + 1
        if normalize == True:
            filename += '_normalized'
        processes = min((ed-st), cpu_num*6)
        qys = np.zeros((processes, 2)).astype(np.int32)
        num = (ed-st)/processes
        for i in range(processes):
            qys[i,:] = np.array((int(st+i*num),int(st+(i+1)*num)))
            kwargs = [aberrations, filename, mode,thresh,normalize,qys[i,:]]
            kwargs_list.append(kwargs)
        print(qys)
        with ProcessPoolExecutor(max_workers=cpu_num) as executor:
            executor.map(self.SSB, (kwarg[0] for kwarg in kwargs_list),
                                   (kwarg[1] for kwarg in kwargs_list),
                                   (kwarg[2] for kwarg in kwargs_list),
                                   (kwarg[3] for kwarg in kwargs_list),
                                   (kwarg[4] for kwarg in kwargs_list),
                                   (kwarg[5] for kwarg in kwargs_list))
        for j in range(processes):
            phi_phase[qys[j, 0]:qys[j, 1], :] = hs.load(f'{self.processname}phase_SSB_{mode}{qys[j,0]}.tif').data
            phi_abs[qys[j, 0]:qys[j, 1], :] = hs.load(f'{self.processname}abs_SSB_{mode}{qys[j,0]}.tif').data
            os.remove(f'{self.processname}phase_SSB_{mode}{qys[j,0]}.tif')
            os.remove(f'{self.processname}abs_SSB_{mode}{qys[j,0]}.tif')
        Image.fromarray(phi_abs).save(f'{self.processname}abs2_SSB_{mode}{filename}{thresh}.tif')
        Image.fromarray(phi_phase).save(f'{self.processname}phase2_SSB_{mode}{filename}{thresh}.tif')
        phi = phi_abs * np.exp(1j*phi_phase)
        phi = np.fft.ifft2(np.fft.ifftshift(phi))
        phi_abs, phi_phase = self.calc_abs_phase(phi,wrapped=True)
        Image.fromarray(phi_abs).save(f'{self.processname}Phi_abs_SSB_{mode}{filename}{thresh}.tif')
        Image.fromarray(phi_phase).save(f'{self.processname}Phi_phase_SSB_{mode}{filename}{thresh}.tif')
