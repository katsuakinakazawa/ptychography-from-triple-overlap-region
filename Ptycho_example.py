#!/usr/bin/env python
# coding: utf-8
#import stem4d as stem4d
import Ptychography as Ptychography


foldername = "insert here your folder name"
pty = Ptychography.Ptychograph(foldername)
pty.kx = 132
pty.ky = 132
pty.FFT4D()
pty.make_sum_image()
pty.measure_center_and_radius()
pty.calc_distanceratio_and_rotation(132,162,128,98,132,132)
Qs = ((111,128),(145,128),(119,143),(119,113),(137, 113),(137,143))
Gs = pty.make_Gs(Qs)
aberrations = pty.measure_aberration(Qs, Gs, region=double, mode='inverse')
pty.SSB_multi(np.zeros(12),'','triple',0.1)
